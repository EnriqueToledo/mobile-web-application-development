<?php

$IP="local";
$options = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);


if($IP=="local")
{
    $dsn = 'mysql:host=localhost;port=3306;dbname=emt16c';
    $username = 'root';
    $password = 'mysql';
}


else
{
    $dsn = 'mysql:host=localhost;port=3306;dbname=yourdbname';
    $username = 'yourusername';
    $password = 'youpassword';
}

try
{

    $db = new PDO($dsn, $username, $password, $options);

}
catch (PDOException $e)
{



 $error = $e->getMessage();
 include('error.php');

 exit();
}
?>