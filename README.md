> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--othe$
>

# LIS4381 - Mobile Web Application Development

## Enrique Toledo

## LIS4381 Requirements: 

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Install JDK
    - Install Android Studio and create My First App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create Healthy Recipes Android app
    - Provide screenshots of completed app
    - Provide screenshots of skill sets

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create ERD based upon business rules
    - Provide screenshot of completed ERD
    - Provide DB resource links
    - Provide My Event app screenshots
    - Provide screenshots fo skills sets

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Clone student files into lis4381 directory
    - Create an online portfolio web application
    - Modify index.php file
    - Provide screenshot of application and skillsets.

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Test server side validation in php file
    - Add data to an existing form
    - Screenshots of skill sets

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create My Business Card application
    - Provide screenshots of completed application
    - Provide screenshots of skill sets

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Suitably modify meta tags
    - Change titles, navigation links, and header tags appropriately
    - Turn of client side validation
    - Add server side validation and regular expressions
    - Create a simple rss feed
    - Answer chapter questions (Ch 13 & 14)
