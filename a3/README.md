> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Enrique Toledo

### Assignment 3 Requirements:

*Five Parts:*

1. Create a petstore database and forward engineer.
2. Provide screenshots of ERD.
3. Create My Event app and provide screenshots.
4. Provide screenshots of skill sets
5. Answer chapter questions (Ch 5-6)

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1
* Screenshot of ERD
* Screenshot of running application's first user interface
* Screenshot of running application's second user interface
* Screenshots of skill sets 1-3
* Links to the following files:
    * [a3.mwb](docs/a3_lis4381.mwb "a3.mwb")
    * [a3.sql](docs/a3.sql "a3.sql")

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

| ERD                 |
|---------------------|
| ![erd](img/erd.png) |

| My Event                          |                                    |
|-----------------------------------|------------------------------------|
| ![myevent app](img/myevent_1.png) | ![myevent app_2](img/myevent_2.png) |

| Skill Set 4 - Decision Structures | Skill Set 5 - Random Array  | Skill Set 6 - Methods       |
|-----------------------------------|-----------------------------|-----------------------------|
| ![Skill set 4](img/ss4.png)       | ![Skill set 3](img/ss5.png) | ![Skill set 6](img/ss6.png) |


