> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Enrique Toledo

### Assignment 2 Requirements:

*Four Parts:*

1. Create a mobile recipe app using Andorid Studio
2. Provide screenshots of completed app
3. Provide screenshots of skill sets
4. Answer chapter questions (Ch 3-4)

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1
* Screenshot of running application's first user interface
* Screenshot of running application's second user interface
* Screenshots of skill sets 1-3

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

| Screenshot of Healthy Recipes Application              |                                                        |
|--------------------------------------------------------|--------------------------------------------------------|
| ![healthy recipes screen 1](img/healthy_recipes_1.png) | ![healthy recipes screen 2](img/healthy_recipes_2.png) |

| Skill Set 1 - Even or Odd                      | Skill Set 2 - Largest of Two Integers                      | Skill Set 3 - Arrays and Loops                      |
|------------------------------------------------|------------------------------------------------------------|-----------------------------------------------------|
| ![Skill set 1](img/even_or_odd_screenshot.png) | ![Skill set 2](img/largest_of_two_integers_screenshot.png) | ![Skill set 3](img/arrays_and_loops_screenshot.png) |


