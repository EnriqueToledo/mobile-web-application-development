public class Main
{
    public static void main(String[] args)
    {
        Methods.getRequirements();
        Methods.forLoop();
        Methods.enhancedForLoop();
        Methods.whileLoop();
        Methods.doWhileLoop();
    }
}