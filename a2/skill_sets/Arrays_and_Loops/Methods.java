public class Methods {
    
    public static void getRequirements()
    {
        System.out.println("Developer: Enrique Toledo");
        System.out.println("Program loops through array of strings");
        System.out.println("Use following values: dog, cat, bird, fish, insect");
        System.out.println("Use following loop structures: for, enhanced for, while, do...while\n");
        System.out.println("Note: Pretest loops: for, enhanced for, while. Posttest loop: do...while.");
        System.out.println();
    }

    public static void forLoop()
    {
        System.out.println("for loop:");
        String[] animals = {"dog", "cat", "bird", "fish", "insect"};

        int i;

        for(i = 0; i <= 4; i++)
        {
            System.out.println(animals[i]);
        }
    }

    public static void enhancedForLoop()
    {
        System.out.println("\nEnhanced for loop:");
        String[] animals = {"dog", "cat", "bird", "fish", "insect"};

        for (String element : animals)
        {
            System.out.println(element);
        }
    }

    public static void whileLoop()
    {
        System.out.println("\nwhile loop:");
        String[] animals = {"dog", "cat", "bird", "fish", "insect"};

        int i = 0;

        while (i < 5 && i <= 5)
        {
            
            System.out.println(animals[i]);
            i++;
        }
    }

    public static void doWhileLoop()
    {
        System.out.println("\ndo...while loop:");
        String[] animals = {"dog", "cat", "bird", "fish", "insect"};

        int i = 0;
        
        do{
            System.out.println(animals[i]);
        
            i++;
        }while (i < 5);

        System.out.println("\n");
    }



}
