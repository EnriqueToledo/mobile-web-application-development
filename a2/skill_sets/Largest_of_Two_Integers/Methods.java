import java.util.Scanner;

public class Methods {
    
    public static void getRequirements()
    {
        System.out.println("Developer: Enrique Toledo");
        System.out.println("Program evaluates largest of two integers");
        System.out.println("Note: Program does *not* check for non-numeric characters or non-integer values.");
        System.out.println();
    }

    public static void evaluateLargestNumber()
    {
        int x = 0;
        int y = 0;
        System.out.print("Enter first integer: ");
        Scanner sc = new Scanner(System.in);
        x = sc.nextInt();

        System.out.print("Enter second integer: ");
        y = sc.nextInt();

        System.out.println(" ");

        if (x > y)
        {
            System.out.println(x + " is larger than " + y);
        } else if (y > x)
        {
            System.out.println(y + " is larger than " + x);
        } else if ( x == y)
        {
            System.out.println("Integers are equal");
        }
    }
}
