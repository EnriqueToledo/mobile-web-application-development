> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Enrique Toledo

### Project 2:

*Six Parts:*

1. Suitably modify meta tags
2. Change titles, navigation links, and header tags appropriately
3. Turn of client side validation
4. Add server side validation and regular expressions
5. Create a simple rss feed
6. Answer chapter questions (Ch 13 & 14)

#### README.md file should include the following items:

* Coursetitle, your name, assignment requirements, as per A1
* Screenshot of index.php
* Screenshots of edit_petstore.php
* Screenshot of Carousel
* Screenshot of simple rss feed

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

| Carousel                                 |
|------------------------------------------|
| ![carousel screenshot](img/carousel.png) |

| Index.php                                  |
|--------------------------------------------|
| ![index.php screenshot](img/index.php.png) |

| edit_petstore.php                                          | 
|------------------------------------------------------------|
| ![edit_petstore.php screenshot](img/edit_petstore.php.png) | 

| Failed Validation                                          |
|------------------------------------------------------------|
| ![failed_validation screenshot](img/failed_validation.png) |

| Passed Validation                                          |
|------------------------------------------------------------|
| ![passed validation screenshot](img/passed_validation.png) |

| Delete Record Prompt                                             |
|------------------------------------------------------------------|
| ![delete record prompt screenshot](img/delete_record_prompt.png) | 

| Successfully Deleted Record                                        |
|--------------------------------------------------------------------|
| ![successfully deleted record](img/successfully_delete_record.png) |

| BBC RSS Feed                             |
|------------------------------------------|
| ![BBC Feed screenshot](img/rss_feed.png) |

