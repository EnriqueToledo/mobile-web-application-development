> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Enrique Toledo

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1,2)

#### README.md file should include the following items:

* Screenshots of AMPPS Installation [My PHP Installation](img/ampps_screenshot.png)
* Screenshot of running java Hello
* Screenshot of running Android Studio - My First App
* git commands w/short descriptions
* Bitbucket repo links: a) this assignment and b) the completed tutorials above (bitbucketstationlocations)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one.
2. git status - Show the working tree status.
3. git add - Add file content to the index.
4. git commit - Record changes to the repository.
5. git push - Update remote refs along with associated objects.
6. git pull - Fetch from and integrate with another repository or a local branch.
7. git merge - Join two or more development histories together.

#### Assignment Screenshots:

| *Screenshot of AMPPS running [My PHP Installation.](img/ampps_screenshot.png)* | *Screenshot of Android Studio - My First App*:                             |
|--------------------------------------------------------------------------------|----------------------------------------------------------------------------|
| ![AMPPS Installation Screenshot](img/ampps_screenshot.png)                     | ![Android Studio Installation Screenshot](img/my_first_app_screenshot.png) |
| *Screenshot of running java Hello*:                                            |                                                                            |
| ![JDK Installation Screenshot](img/java_hello_screenshot.png)                  |                                                                            |


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/EnriqueToledo/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")
