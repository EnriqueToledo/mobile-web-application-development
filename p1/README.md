> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Enrique Toledo

### Project 1 Requirements:

*Three Parts:*

1. Create My Business Card app and provide screenshots.
2. Provide screenshots of skill sets
3. Answer chapter questions (Ch 7 & 8)

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1
* Screenshot of running application's first user interface
* Screenshot of running application's second user interface
* Screenshots of skill sets 7-9

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

| My Business Card application                      |                                                   |
|---------------------------------------------------|---------------------------------------------------|
| ![My business card app screenshot](img/app_1.png) | ![My business card app screenshot](img/app_2.png) |

| Skill Set 7 - Random Array Using Methods and Data Validation | Skill Set 8 - Largest of Three Integers   |
|--------------------------------------------------------------|-------------------------------------------|
| ![ss7 screenshot](img/ss7_screenshot.png)                    | ![ss8 screenshot](img/ss8_screenshot.png) |

| Skill Set 9 - Array Runtime Data Validation |
|---------------------------------------------|
| ![ss9 screenshot](img/ss9_screenshot.png)   |
