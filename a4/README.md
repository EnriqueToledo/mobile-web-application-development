> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Enrique Toledo

### Assignment 4  Requirements:

*Three Parts:*

1. Modify index.php to create my online portolio web application
2. Provide screenshots of skill sets
3. Answer chapter questions (Ch 9, 10, & 15)

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1
* Screenshot of running Main Page web application
* Screenshot of running failed validation and successful validation in web application.
* Link to local lis4381 web app: [http://localhost/repos/lis4381/](http://localhost/repos/lis4381/ "http://localhost/repos/lis4381/")
* Screenshots of skill sets 10-12

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

| Portal (Main Page)                             |
|------------------------------------------------|
| ![Portal (Main Page) screenshot](img/main.png) |

| Failed validation                                 | Passed validation                                |
|---------------------------------------------------|--------------------------------------------------|
| ![failed validation screenshot](img/a4_error.png) | ![passed validation screenshot](img/a4_pass.png) |


| Skill Set 10 - Java: Array Lists               |
|------------------------------------------------|
| ![ss10_screenshot](img/ss10.png)               |

| Skill Set 12 - Java: Nested Structures |                                      |
|----------------------------------------|--------------------------------------|
| ![ss11 screenshot](img/ss11_1.png)     | ![ss11 screenshot 2](img/ss11_2.png) |

| Skill Set 12 - Java: Temperature Conversion Program |
|-----------------------------------------------------|
| ![ss12 screenshot](img/ss12.png)                    |
