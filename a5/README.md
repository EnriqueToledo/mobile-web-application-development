> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Enrique Toledo

### Assignment 5 Requirements:

*Four Parts:*

1. Test server side validation in add_petstore.php
2. Add data to an existing form
3. Answer chapter questions (Ch 11, 12, & 19)
4. Skill Sets 13 - 15

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1
* Screenshots of a5 petstore data
* Screenshot of server side validation
* Screenshots of skill sets 13 - 15

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

| Index.php                                  |
|--------------------------------------------|
| ![index.php screenshot](img/index.php.png) |

| Skill Set 13 - Java: Sphere Volume Calculator |
|-----------------------------------------|
| ![skill set 13](img/ss13.png)           | 

| Skill Set 14 - PHP: Simple Calculator (Addition) |                                             |
|--------------------------------------------------|---------------------------------------------|
| ![ss14_screenshot](img/simple_calc_1.png)        | ![ss14_2_screenshot](img/simple_calc_2.png) |

| Skill Set 14 - PHP: Simple Calculator (Division) |                                             |
|--------------------------------------------------|---------------------------------------------|
| ![ss14_3_screenshot](img/simple_calc_3.png)      | ![ss14_4_screenshot](img/simple_calc_4.png) |

| Skill Set 15 - PHP: Write/Read File |                                      |
|-------------------------------------|--------------------------------------|
| ![ss15_screenshot](img/ss15_1.png)  | ![ss15_2_screenshot](img/ss15_2.png) |
